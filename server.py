#!/usr/bin/env python

import json
import linuxcnc
from websocker_server import WebsocketServer

# command = linuxcnc.command()


class WSServer:

    def __init__(self, port=1337):

        self.port = port
        self.server = WebsocketServer(self.port)
        self.server.set_fn_new_client(self.new_client)
        self.server.set_fn_client_left(self.client_left)
        self.server.set_fn_message_received(self.message_received)

        self.lcnc_command = linuxcnc.command()

    def run(self):
        self.server.run_forever()

    # Called for every client connecting (after handshake)
    def new_client(self, client, server):
        client_id = client['id']
        client_address = client['address']
        print("New client connected and was given id {0}".format(client_id))
        server.send_message(client, "This client's ID is {0} and has address {1}".format(client_id, client_address))
        server.send_message_to_all("Hey all, a new client has joined us")

    # Called for every client disconnecting
    def client_left(self, client, server):
        print("Client({0}) disconnected".format(client.get(id)))

    # Called when a client sends a message
    def message_received(self, client, server, message):
        parsed_msg = json.loads(message)

        print(json.dumps(parsed_msg, indent=4, sort_keys=True))

        self.process_command(client, parsed_msg)

    def process_command(self, client, data):
        method_type = data.get('type')
        if method_type == "command":

            command_method = data.get('method')

            if hasattr(self.lcnc_command, command_method):
                try:
                    cmd = getattr(self.lcnc_command, command_method)
                    command_args = data.get('args')
                    cmd(command_args)
                    self.server.send_message(client, "CMD: {} success".format(command_method))

                except Exception as e:
                    self.server.send_message(client, "ERROR: {}".format(e))
            else:

                self.server.send_message(client, "ERROR: method {} not found".format(command_method))

