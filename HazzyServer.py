import logging

from server import WSServer

FORMAT = "[%(name)s][%(levelname)s]  %(message)s (%(filename)s:%(lineno)d)"

logging.basicConfig(filename='hazzy-server.log', level=logging.DEBUG, format=FORMAT)


def main():
    server = WSServer(port=9001)
    server.run()


if __name__ == "__main__":
    main()
